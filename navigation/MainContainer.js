import * as React from 'react'
import { StyleSheet, Image, Text, View, TouchableOpacity, SafeAreaView, Button } from 'react-native';
import 'react-native-gesture-handler'
import 'localstorage-polyfill'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

//screens
import HomeScreen from '../components/HomeScreen'
import ConnexionScreen from '../components/ConnexionScreen'
import InscriptionScreen from '../components/InscriptionScreen'
import WelcomePage from '../components/WelcomePage'
import ProfilePage from '../components/ProfilePage'
import DocumentsPage from '../components/MyDocumentsPage'

// screens names
const homeName = 'Home'
const inscriptionName = 'Inscription'
const connexionName = 'Connexion'
const documentName = 'DocumentsPage'
const welcomeName = 'WelcomePage'
const profileName = 'ProfilePage'

// npm install --save react-native-gesture-handler react-native-reanimated react-native-screens
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function HomeTabs() {
    return (
      <Tab.Navigator>
        <Tab.Screen name={connexionName} component={ConnexionScreen} />
        <Tab.Screen name={inscriptionName} component={InscriptionScreen} />
      </Tab.Navigator>
    );
  }

export default function MainContainer() {
    onPress1 = () => {
        navigation.navigate('Connexion')
      };
    onPress2 = () => {
        navigation.navigate('Inscription')
      };
  return (
    <SafeAreaView style={styles.container}>
        <Image source={require('../snack-static/Accueil.png')} 
        style={{ width: 470, height: 330 }}
        />
        <Image source={require('../snack-static/Hospitech-logo.png')} 
        style={{ width: 300, height: 100, margin: 40}}
        />
      <View>
        
            <TouchableOpacity
              style={styles.button}
              onPress={onPress1}
            >
              <Text>Connexion</Text>
            </TouchableOpacity>
      </View>
            <View>
            <TouchableOpacity
              style={styles.button}
              onPress={onPress2}
            >
              <Text>Inscription</Text>
            </TouchableOpacity>
          </View>
          </SafeAreaView>,
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName={homeName}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName
            let rn = route.name
            if (rn === homeName) {
              iconName = focused ? 'home' : 'home-outline'
            } else if (rn === inscriptionName) {
              iconName = focused ? 'settings' : 'settings-outline'
            } else if (rn === connexionName) {
              iconName = focused ? 'settings' : 'settings-outline'
            }
            return <Ionicons name={iconName} size={size} color={color} />
          },
        })}
        screenOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'grey',
          labelStyles: { paddingBottom: 10, fontSize: 10 },
        }}
      >
        <Tab.Screen name={homeName} component={HomeTabs} />
        <Tab.Screen name={inscriptionName} component={InscriptionScreen} />
        <Tab.Screen name={connexionName} component={ConnexionScreen} />
        <Tab.Screen name={profileName} component={ProfilePage} />
        <Tab.Screen name={documentName} component={DocumentsPage} />
      </Tab.Navigator>
    </NavigationContainer>
  )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      padding: 20,
      margin: 10,
    },
    title: {
      textAlign: 'center',
      marginVertical: 8,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#A4BBE8",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        padding: 20,
        width: 300,
        margin: 10,
      },
  });