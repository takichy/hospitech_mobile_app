import React from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import * as ImagePicker from 'expo-image-picker';

class AddDoc extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      avatar_blob_blob: ''
    }

    this.chooseFile = this.chooseFile.bind(this)
  }

  chooseFile = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [4, 3],
      quality: 1
    });
    this.setState({
      avatar: { uri: result.uri }
    })
    this.props.dataFromParent(this.state.avatar_blob.uri);
  };

  urlToBlob = async(imageUri) => {
    const response = await fetch(imageUri);
    const blob = await response.blob();
    return blob;
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.touchableOpacity}
        onPress={this.chooseFile}>
      </TouchableOpacity>

    )
  }
}

const styles = StyleSheet.create({
  touchableOpacity: {
    margin: 5,
    width: 200, // Pensez bien à définir une largeur ici, sinon toute la largeur de l'écran sera cliquable
    height: 150,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonStyle: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#DDDDDD',
    padding: 5,
  },
  avatar_blob: {
    width: 200,
    height: 150,
    borderRadius: 0,
    borderColor: '#9B9B9B'
  },
  avatar_container: {
    alignItems: 'center',
  }
})

export default AddDoc