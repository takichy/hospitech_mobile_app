import React, { useState } from 'react'
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  Button
} from 'react-native'
import AuthService from '../services/auth-service'
import Spinner from 'react-native-loading-spinner-overlay'
import 'localstorage-polyfill'
import { render } from 'react-dom'
import { isLoading } from 'expo-font'

export default class ConnexionScreen extends React.Component {

  constructor(props) {
    super(props)

    this.handleLogin = this.handleLogin.bind(this)
    this.onChangeEmail = this.onChangeEmail.bind(this)
    this.onChangePassword = this.onChangePassword.bind(this)

    this.state = {
      username: '',
      firstname: '',
      lastname: '',
      age: '',
      email: '',
      NSS: '',
      address: '',
      favorite: '',
      password: '',
      successful: false,
      message: '',
      ErrorMessageEmail: '',
      ErrorMessagePassword: '',
      isLoading: false,
      isEmailValid: '',
      isPasswordOK: false,
      PasswordStrength: '',
      backgroundColor: '#fff',
      ErrorMessageGeneral: '',
      isConnexionOK: false
    }
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value,
    })
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value,
    })
  }

  async handleLogin(e) {
    e.preventDefault()

    this.setState({
      message: '',
      isLoading: true
    })
    if (this.state.email != "" && this.state.password != "") {
      await AuthService.login(this.state.email, this.state.password)
        .then(
          (response) => {
            this.setState({
              message: response.data.message,
              token: response.data.content.token,
              isPasswordOK: true,
              isConnexionOK: true,
              isLoading: false
            })
            if (response.status == 200) {
              localStorage.setItem('userToken', response.data.content.token);
              this.props.navigation.navigate('SignedIn');
            }
            else {
              this.setState({
                isLoading: false,
                successful: false
              });
              this.connexionValidation();
            }
          },

        )
        .catch((error) => {
          this.setState({
          isLoading: false,
          successful: false
        });
          //console.log(error);
          this.connexionValidation();
        })
    }
    else {
      this.setState({
        isLoading: false,
      })
      this.emailValidationRegex(this.state.email);
      this.passwordValidation(this.state.password);
    }
  }

  goToSubscribePage = () => {
    this.props.navigation.navigate('Inscription')
  }

  emailValidationRegex(email) {
    let regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email == "") {
      this.setState({
        ErrorMessageEmail: "Please enter an Email"
      })
      return false;
    }
    if (!regEmail.test(email)) {
      this.setState({
        ErrorMessageEmail: "Sorry! Invalid Email"
      })
      return false;
    }
  }

  passwordValidation(password) {
    if (password == "") {
      this.setState({
        ErrorMessagePassword: "Please set a Password",
        PasswordStrength: ""
      })
      return false;
    }
  }

  connexionValidation() {
    this.setState({
      ErrorMessageGeneral: "Wrong email or password"
    })
    return false;
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Image
          source={require('../snack-static/H.png')}
          style={{ width: 100, height: 80, margin: 40 }}
        />

        <Spinner visible={this.state.isLoading} />
        <View>
          <Text>Email :</Text>
          <TextInput
            style={styles.input}
            placeholder="  Email"
            keyboardType="email-address"
            name="email"
            onChangeText={(email) => {
              this.setState({
                email: email,
                isEmailValid: this.emailValidationRegex(email)
              })
            }}
            defaultValue={this.state.email}
          />
          {(this.state.isEmailValid == false) && <Text style={styles.ErrorMessageStyle}>{this.state.ErrorMessageEmail}</Text>}
          <Text>Mot de passe :</Text>
          <TextInput
            style={styles.input}
            placeholder="Mot de passe"
            secureTextEntry={true}
            name="password"
            onChangeText={(password) => {
              this.setState({
                password: password,
                isPasswordOK: this.passwordValidation(password)
              }), this.checkPassword
            }}
            defaultValue={this.state.password}
          />
          {(this.state.isPasswordOK == false) && <Text style={styles.ErrorMessageStyle}>{this.state.ErrorMessagePassword}</Text>}
          {(this.state.isConnexionOK == false) && <Text style={styles.ErrorMessageStyle}>{this.state.ErrorMessageGeneral}</Text>}
          <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
            <Text>Valider</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.goToSubscribePage}>
            <Text>S'inscrire</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}

// ...

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    margin: 10,
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#A4BBE8',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    padding: 20,
    margin: 10,
  },
  input: {
    height: 40,
    width: 300,
    margin: 12,
    borderWidth: 1,
  },
  inputPassword: {
    height: 40,
    width: 300,
    margin: 12,
    borderWidth: 1
  },
  ErrorMessageStyle: {
    color: '#FF0000'
  },

})
