import React from 'react'
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Button,
} from 'react-native'
import AuthService from '../services/auth-service'
import 'localstorage-polyfill'
import { render } from 'react-dom'

export default class AdmissionPage extends React.Component {
  constructor(props) {
    super(props)
  }
  render()
  {
    return (
      <SafeAreaView style={styles.container}>
        <Image
          source={require('../snack-static/H.png')}
          style={{ width: 100, height: 80, margin: 40 }}
        />
        <Text>Mes admissions</Text>
      </SafeAreaView>
    )
  }
}

// ...

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    margin: 0,
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#A4BBE8',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    padding: 20,
    width: 300,
    margin: 10,
  },
})
