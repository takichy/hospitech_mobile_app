import React from 'react'
import {
  StyleSheet,
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Text,
  FlatList,
  Alert,
  LogBox
} from 'react-native'
import Moment from 'moment';
import * as FileSystem from 'expo-file-system';
import * as IntentLauncher from 'expo-intent-launcher';
import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';
import AuthService from '../services/auth-service'
import Spinner from 'react-native-loading-spinner-overlay'

import 'localstorage-polyfill'
//import Card from 'react-bootstrap/Card'
import { Card, Divider } from 'react-native-elements';
export default class MyDocumentsPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      avatar_blob: '',
      user_id: '',
      email: '',
      document_name: '',
      document_type: '',
      document_uri: '',
      imageByte: '',
      isLoading: false,
      list_docs: []
    }
    this.urlToBlob = this.urlToBlob.bind(this)
    this.updateDoc = this.updateDoc.bind(this)
    this.openDoc = this.openDoc.bind(this)
    this.deleteOne = this.deleteOne.bind(this)
  }

  async componentDidMount() {
    await this.getUser();
    await this.getDocs(this.state.user_id);
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevState.list_docs !== this.state.list_docs) {
      await this.getDocs(this.state.user_id)
    }
    this.props.navigation.addListener('tabPress', (route) => { this.getUser(), this.getDocs(this.state.user_id) });
  }

  async componentWillUnmount() {
    this.props.navigation.addListener('tabPress', (route) => { this.getUser(), this.getDocs(this.state.user_id) });
  }

  async getDocs(id_user) {
    this.setState({
      isLoading: false
    }),
      await AuthService.getAllDocs(id_user).then(response => {
        this.setState({
          list_docs: response,
          isLoading: false
        });
      }).catch(err => console.error(err));
  }

  async getUser() {
    this.setState({
      isLoading: true
    })
    await AuthService.getCurrentUser().then(response => {
      this.setState({
        email: response.user.email,
        user_id: response.user._id,
        isLoading: false
      })
    }).catch(err => console.error(err))
  }

  openImagePickerAsync = async () => {
    let permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!"); return;
    }
    let pickerResult = await DocumentPicker.getDocumentAsync();
    if (pickerResult.cancelled === true) {
      console.log('User cancelled photo picker');
      Alert.alert('You did not select any image');
    }

    Alert.alert('', 'Are you sure you want to choose this Image ?', [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      {
        text: "YES",
        onPress: () => this.urlToBlob(pickerResult)
      }
    ]);
  }

  urlToBlob = async (pickerResult) => {
    const filename = pickerResult.name;
    const typeFile = filename.split('.').pop();
    await FileSystem.readAsStringAsync(pickerResult.uri, { encoding: 'base64' })
      .then(base64 => {
        this.setState({
          document_name: filename,
          document_uri: pickerResult.uri,
          document_type: typeFile,
          imageByte: base64
        }),
          this.updateDoc();
      });
  }

  updateDoc = async () => {
    const data =
    {
      'id_user': this.state.user_id,
      'document_blob': this.state.imageByte,
      'document_uri': this.state.document_uri,
      'document_type': this.state.document_type,
      'document_name': this.state.document_name,
      'creation_date': Moment(new Date())
    }

    this.setState({
      isLoading: true
    });
    await AuthService.createDocuments(data).then(
      this.setState({
        isLoading: false
      }),
      await this.getDocs(this.state.user_id));
  }

  deleteOne = async (id_doc) => {
    this.setState({
      isLoading: true
    });
    await AuthService.deleteDocuments(id_doc).then(
      this.setState({
        isLoading: false
      }),
      await this.getDocs(this.state.user_id),
      this.forceUpdate());
  }


  openDoc = async (uri) => {
    try {
      const cUri = await FileSystem.getContentUriAsync(uri);

      await IntentLauncher.startActivityAsync("android.intent.action.VIEW", {
        data: cUri,
        flags: 1,
      });

    } catch (e) {
      console.log(e.message);
    }
  }

  handleCallback = (childData) => {
    this.setState({ avatar_blob: childData })
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Spinner visible={this.state.isLoading} />
        <View style={styles.scrollView}>
          <FlatList
            ListHeaderComponent={<>
              <Image
                source={require('../snack-static/H.png')}
                style={styles.logo} /><Text style={styles.title}>Mes documents</Text>
              </>}
            data={this.state.list_docs}
            horizontal={false}
            showsVerticalScrollIndicator={false}
            numColumns={2}
            style={{ flexGrow: 1,margin:0 ,paddingBottom: "20%", zIndex: 0, position:'relative'}}
            renderItem={({ item }) => <>
              <Card containerStyle={{ width: "42%", height: "100%", position:'relative' }} titleNumberOfLines="2">
                <Card.Title>{item.document_name} </Card.Title>
                <Card.Divider />
                <Card.Image
                  onPress={() => {this.openDoc(item.document_uri)}}
                  source={(item.document_type == 'jpg' || item.document_type == 'png') ? { uri: `data:image/gif;base64,${item.document_blob}` } : require('../snack-static/pdf.png')} />
                <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 10}}>
                <TouchableOpacity onPress={() => { this.openDoc(item.document_uri) }} >
                  <Ionicons name="eye" size={36} color="#0a3c53" style={{ marginRight: '30%' }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.deleteOne(item._id) }}>
                  <Ionicons name="trash" size={36} color="#8d3f3f" />
                </TouchableOpacity>
                </View >
              </Card>
              <Divider orientation="horizontal" width={5} />
            </>}
            keyExtractor={item => item._id}
            ListFooterComponent={<><Image
              style={{ width: 100, height: 80, margin: 40 }} />
              <TouchableOpacity onPress={this.openImagePickerAsync} style={{ zIndex:4, right: 10, bottom: 45, position:'absolute', }}>
                <Ionicons name="md-add-circle" size={56} color="#96c193"  />
              </TouchableOpacity></>}
          >
          </FlatList>
        </View>
      </SafeAreaView>
    )
  }
}

// ...

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    position:'relative'
  },
  logo: {
    alignSelf: 'center',
    width: 100,
    height: 80,
    marginTop: 60,
    marginBottom: 40
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#A4BBE8',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    padding: 20,
    width: 300,
    margin: 10,
  },
  touchableOpacity: {
    margin: 5,
    width: 200, // Pensez bien à définir une largeur ici, sinon toute la largeur de l'écran sera cliquable
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red'
  },
  buttonStyle: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#DDDDDD',
    padding: 5,
  },
  avatar_blob: {
    width: 200,
    height: 150,
    borderRadius: 0,
    borderColor: '#9B9B9B'
  },
  avatar_container: {
    alignItems: 'center',
  },
  icon_file: {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: "red"
  },
  scrollView: {
    backgroundColor: '#fff',
    zIndex:0,
    position:'relative'

  },
})
