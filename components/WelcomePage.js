import React from 'react'
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Button,
} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import AuthService from '../services/auth-service'
import 'localstorage-polyfill'

export default class WelcomePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      firstname: '',
      lastname: '',
      age: '',
      email: '',
      NSS: '',
      address: '',
      favorite: '',
      password: '',
      successful: false,
      message: '',
      isLoading: false
    }
  }
  async componentDidMount() {
    await this.getUser();
  }

  async getUser() {
    this.setState({
      isLoading: true
    })
    AuthService.getCsrfToken();
    await AuthService.getCurrentUser()
      .then(response => {
        this.setState({
          email: response.user.email,
          firstname: response.user.firstname,
          lastname: response.user.lastname,
          successful: true,
          isLoading: false
        });
      }
      )
      .catch(err => console.error(err));

  }

  componentWillUnmount() {
    this.props.navigation.addListener('tabPress', (route) => { });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Image
          source={require('../snack-static/H.png')}
          style={styles.logo}
        />
        <Spinner visible={this.state.isLoading} />
        <Text style={styles.title}> Hello {this.state.firstname}</Text>
        <Text style={styles.subTitle}> Busy times</Text>
        <Image
          source={require('../snack-static/affluenceImage.png')}
          style={styles.png}
        />
      </SafeAreaView>
    )
  }
}

// ...

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  logo: {
    alignSelf: 'center',
    width: 100,
    height: 80,
    marginTop: 60,
    marginBottom: 40
  },
  title: {
    textAlign: 'center',
    textTransform: 'capitalize',
    fontWeight: 'bold',
    fontSize: 32
  },
  subTitle: {
    textAlign: 'left',
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    textTransform: 'capitalize',
    paddingLeft: 30,
    marginTop: 30,
  },
  png: {
    width: 300,
    height: 200,
    alignSelf: 'center'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#A4BBE8',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    padding: 20,
    width: 300,
    margin: 10,
  }
})
