import React, { useState } from 'react'
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
  SafeAreaView,
  LogBox
} from 'react-native'
import Moment, { relativeTimeThreshold } from 'moment';
import DatePicker from 'react-native-datepicker';
import { TextInputMask } from 'react-native-masked-text'
import AuthService from '../services/auth-service'
import Avatar from './Avatar'
import Spinner from 'react-native-loading-spinner-overlay'
import 'localstorage-polyfill'
import * as FileSystem from 'expo-file-system';
import bcrypt from 'react-native-bcrypt';
import isaac from "isaac";

export default class ProfilePage extends React.Component {
  constructor(props) {
    super(props)
    this.handleMAJ = this.handleMAJ.bind(this)
    this.handlerUserAvatar = this.handlerUserAvatar.bind(this);

    this.state = {
      myuser: {},
      idUser: '',
      firstname: '',
      lastname: '',
      birthdate: '',
      birthdateFormat: '',
      email: '',
      num_ss: '',
      address: '',
      favorite: '',
      password: '',
      passwordRepeated: '',
      currentPasswordUser: '',
      successful: false,
      message: '',
      isLoading: false,
      ErrorMessageEmail: '',
      ErrorMessagePassword: '',
      ErrorMessagePasswordRepeated: '',
      isEmailValid: '',
      isPasswordOK: false,
      isPasswordRepeatedOK: false,
      PasswordStrength: '',
      PasswordStrengthRepeated: '',
      backgroundColor: '#fff',
      backgroundColorRepeated: '#fff',
      avatar_blob: '',
      ErrorMessageNSS: '',
      isNSSOK: false,
      avatarFile: null
    }
  }

  async componentDidMount() {
    LogBox.ignoreLogs([
      'Animated: `useNativeDriver`',
      'DatePickerIOS has been merged with DatePickerAndroid and will be removed in a future release.',
      'StatusBarIOS has been merged with StatusBar and will be removed in a future release.',
      'DatePickerAndroid has been merged with DatePickerIOS and will be removed in a future release.',
      'componentWillReceiveProps',
      'Please update the following components: %s, DatePicker'
    ]);
    await this.getUser();
  }

  async getUser() {
    this.setState({
      isLoading: true
    })
    await AuthService.getCurrentUser().then(response => {
      this.setState({
        email: response.user.email,
        currentPasswordUser: response.user.password,
        firstname: response.user.firstname,
        lastname: response.user.lastname,
        idUser: response.user._id,
        num_ss: response.user.num_ss,
        birthdate: response.user.birthdate,
        successful: true,
        isLoading: false,
        myuser: {
          id: response.user._id,
          email: response.user.email,
          password: response.user.password,
          firstname: response.user.firstname,
          lastname: response.user.lastname,
          birthdate: response.user.birthdate,
          num_ss: response.user.num_ss,
          profilImg: response.user.profilImg
        }
      });
    }).catch(err => console.error(err));

    await AuthService.getUserLinkedImg().then(async (response) => {
      if (response.user.userImageUri) {
        this.setState({
          avatarFile: response.user.userImageUri
        });
      }
    });
  }

  componentDidUpdate() {
    LogBox.ignoreLogs([
      'Animated: `useNativeDriver`',
      'DatePickerIOS has been merged with DatePickerAndroid and will be removed in a future release.',
      'StatusBarIOS has been merged with StatusBar and will be removed in a future release.',
      'DatePickerAndroid has been merged with DatePickerIOS and will be removed in a future release.',
      'componentWillReceiveProps'
    ]); 
    this.props.navigation.addListener('tabPress', (route) => {
      this.getUser(),
        this.setState({
          ErrorMessageNSS: '',
          ErrorMessageEmail: '',
          ErrorMessagePassword: '',
          ErrorMessagePasswordRepeated: ''
        })
    });
  }

  componentWillUnmount() {
    this.props.navigation.addListener('tabPress', (route) => { this.getUser() });
  }

  async handleMAJ(e) {
    e.preventDefault()
    this.setState({
      message: '',
      successful: false,
      isLoading: true,
    });

    bcrypt.setRandomFallback((len) => {
      const buf = new Uint8Array(len);

      return buf.map(() => Math.floor(isaac.random() * 256));
    });

    if (this.state.password != null && this.state.password != "") {
      let password = this.state.password;
      this.state.password = bcrypt.hashSync(password, 10);
    }

    let Obj;
    if (this.state.imageObject) {
      var files = [
        {
          name: 'ProfilePicture_' + this.state.email,
          filename: this.state.imageObject.split(/[\\/]/).pop().replace(/\.[^/.]+$/, "") + "." + this.state.imageObject.split('.').pop(),
          filepath: this.state.imageObject,
          filetype: 'image/jpeg'
        }
      ];

      //upload files
      const imageSent = await AuthService.sendProfilePicture(files, this.state.email, 'health_insurance');
        
      Obj = JSON.parse(imageSent.body);
    }
    let data = {
      email: this.state.email,
      password: this.state.password ? this.state.password : this.state.currentPasswordUser,
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      birthdate: this.state.birthdate,
      num_ss: this.state.num_ss,
      profilImg: this.state.imageObject ? Obj.imagename : this.state.myuser.profilImg
    };

    Object.keys(this.state.myuser).map((key) => {
      if (data[key]) {
        this.state.myuser[key] = data[key]
      }
    });
    AuthService.update(
      this.state.myuser.id,
      this.state.myuser.email,
      this.state.myuser.password,
      this.state.myuser.firstname,
      this.state.myuser.lastname,
      this.state.myuser.birthdate,
      this.state.myuser.num_ss,
      this.state.myuser.profilImg
    )
      .then(
        (response) => {
          this.setState({
            message: response.message,
            successful: true,
            isLoading: false,
            imageObject:""
          }),
          this.props.navigation.navigate('ProfilePage')
        },
        (error) => {
          const resMessage =
            (error.response && error.response.data && error.response.message) ||
            error.message ||
            error.toString()

          this.setState({
            successful: false,
            message: resMessage,
          })
        },
      )
      .catch((error) => {
        console.log(error)
      })

    //this.emailValidationRegex(this.state.email);
    //this.passwordValidation(this.state.password);
  }

  handlerUserAvatar(objAvatar) {
    this.setState({
      imageObject: objAvatar
    })
  }

  goToConnexionPage = () => {
    this.props.navigation.navigate('Connexion')
  }

  emailValidationRegex(email) {
    let regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email == "") {
      this.setState({
        ErrorMessageEmail: "Please enter an Email"
      })
      return false;
    }
    if (!regEmail.test(email)) {
      this.setState({
        ErrorMessageEmail: "Sorry! Invalid Email"
      })
      return false;
    }
  }

  passwordValidation(password) {
    if (password == "") {
      this.setState({
        ErrorMessagePasswordRepeated: "",
        PasswordStrengthRepeated: ""
      })
      return true;
    }

    if (password != this.state.passwordRepeated) {
      this.setState({
        ErrorMessagePasswordRepeated: "Password are not the same",
        PasswordStrengthRepeated: "",
        backgroundColorRepeated: "#bf1b0d"
      })
    }
    else {
      this.setState({
        ErrorMessagePasswordRepeated: "",
        PasswordStrengthRepeated: "",
        backgroundColorRepeated: "#0F9D58"
      })
    }

    const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    //The string must contain at least 1 lowercase alphabetical character
    //The string must contain at least 1 uppercase alphabetical character
    //The string must contain at least 1 numeric character
    //The string must contain at least one special character, but we are escaping reserved RegEx characters to avoid conflict
    //The string must be eight characters or longer
    const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
    //The string must be six characters or longer 
    //The string must contain at least one lowercase and one uppercase alphabetical character 
    //  or has at least one lowercase and one numeric character 
    //  or has at least one uppercase and one numeric character.

    if (strongRegex.test(password)) {
      this.setState({ PasswordStrength: " Strength: Strong", backgroundColor: "#0F9D58" });
    } else if (mediumRegex.test(password)) {
      this.setState({ PasswordStrength: " Strength: Medium", backgroundColor: "#f49200" });
    } else {
      this.setState({ PasswordStrength: " Strength: Low", backgroundColor: "#bf1b0d" });
    }
  }

  passwordRepeatedValidation(passwordRepeated) {
    if (passwordRepeated != this.state.password) {
      this.setState({
        ErrorMessagePasswordRepeated: "Password are not the same",
        PasswordStrengthRepeated: "",
        backgroundColorRepeated: "#bf1b0d"
      })
      return false;
    }
    else {
      this.setState({
        ErrorMessagePasswordRepeated: "",
        PasswordStrengthRepeated: "",
        backgroundColorRepeated: "#0F9D58"
      })
    }
    return true;
  }

  NSSValidation(NSS) {
    if (NSS == "") {
      this.setState({
        ErrorMessageNSS: 'Please set a NSS'
      })
      return false;
    }

    const regex = new RegExp("^[12][0-9]{2}(0[1-9]|1[0-2])(2[AB]|[0-9]{2})[0-9]{3}[0-9]{3}$");

    if (regex.test(NSS) && NSS.length == 13) {
      this.setState({ ErrorMessageNSS: '' });
      return true;
    }
    else if (!regex.test(NSS) && NSS.length == 13) {
      this.setState({ ErrorMessageNSS: 'Enter a valid NSS' });
      return false;
    }
    else if (regex.test(NSS) && NSS.length != 13) {
      this.setState({ ErrorMessageNSS: 'Number of characters required: 13' });
      return false;
    }
    else {
      this.setState({ ErrorMessageNSS: 'Number of characters required: 13' });
      return false;
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          automaticallyAdjustContentInsets="true"
        >
          <View style={styles.scrollView}>
            <Image
              source={require('../snack-static/H.png')}
              style={styles.logo}
            />

            <Spinner visible={this.state.isLoading} />
            <View>
              <Text style={styles.title}>My profile :</Text>
              <Text>Last Name :</Text>
              <TextInput
                style={styles.input}
                placeholder=" Last Name"
                keyboardType="default"
                name="lastname"
                onChangeText={(lastname) => this.setState({ lastname: lastname })}
                defaultValue={this.state.lastname}
              />
              <Text>First Name :</Text>
              <TextInput
                style={styles.input}
                placeholder="  First Name"
                keyboardType="default"
                name="firstname"
                onChangeText={(firstname) => this.setState({ firstname: firstname })}
                defaultValue={this.state.firstname}
              />
              <Text>Date of Birth :</Text>
              <DatePicker
                style={styles.datePickerStyle}
                date={this.state.birthdate} // Initial date from state
                mode="date" // The enum of date, datetime and time
                placeholder="select date"
                format="DD/MM/YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                useNativeDriver="true"
                customStyles={{
                  dateIcon: {
                    //display: 'none',
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 36
                  },
                }}
                onDateChange={(date) => {
                  this.setState({
                    birthdate: date,
                    birthdateFormat: Moment(date, "DD/MM/YYYY")
                  });
                }}
              />
              <Text>Email :</Text>
              <TextInput
                style={styles.input}
                placeholder="  Email"
                keyboardType="email-address"
                name="email"
                onChangeText={(email) => this.setState({ email: email })}
                defaultValue={this.state.email}
              />
              <Text>N° sécurité sociale :</Text>
              <TextInput
                style={styles.input}
                placeholder="  NSS"
                keyboardType="numeric"
                name="NSS"
                defaultValue={this.state.num_ss}
                maxLength={13}
                onChangeText={num_ss => {
                  this.setState({
                    num_ss: num_ss,
                    isNSSOK: this.NSSValidation(num_ss)
                  })
                }}
              />
              {((this.state.isNSSOK == false || this.state.ErrorMessageNSS !== "") && this.state.avatarFile) && <Text style={styles.ErrorMessageStyle}>{this.state.ErrorMessageNSS}</Text>}
              <Text>Ajouter ma mutuelle :</Text>
              <View style={styles.avatar_container} avatarParrent={this.state.avatar}>
                {(this.state.avatarFile) && <Avatar src={this.state.avatarFile} dataFromParent={this.handlerUserAvatar} />}
              </View>
              {/* <Text>Adresse :</Text>
            <TextInput
              style={styles.input}
              placeholder="  Adresse"
              keyboardType="default"
              name="address"
              onChangeText={(address) => this.setState({ address: address })}
              defaultValue={this.state.address}
            />
            <Text>Hôpital favoris :</Text>
            <TextInput
              style={styles.input}
              placeholder="  Hôpital favoris"
              keyboardType="default"
              name="favorite"
              onChangeText={(favorite) => this.setState({ favorite: favorite })}
              defaultValue={this.state.favorite}
            /> */}
              <Text>Mot de passe :</Text>
              <TextInput
                style={{
                  height: 40,
                  margin: 10,
                  borderWidth: 1,
                  borderColor: '#ABA9AB',
                  backgroundColor: this.state.backgroundColor
                }}
                placeholder="  Mot de passe"
                onChangeText={(password) => {
                  this.setState({
                    password: password,
                    isPasswordOK: this.passwordValidation(password)
                  })
                }}
                secureTextEntry={true}
              />
              {(this.state.isPasswordOK == false) && <Text style={styles.ErrorMessageStyle}>{this.state.ErrorMessagePassword}</Text>}
              {(this.state.PasswordStrength !== "") && <Text style={styles.ErrorMessageStyle}>{this.state.PasswordStrength}</Text>}
              <Text>Répéter le mot de passe :</Text>
              <TextInput
                style={{
                  height: 40,
                  margin: 10,
                  borderWidth: 1,
                  borderColor: '#ABA9AB',
                  backgroundColor: this.state.backgroundColorRepeated
                }}
                placeholder="  Répéter le mot de passe"
                secureTextEntry={true}
                name="passwordRepeated"
                onChangeText={(passwordRepeated) => {
                  this.setState({
                    passwordRepeated: passwordRepeated,
                    isPasswordRepeatedOK: this.passwordRepeatedValidation(passwordRepeated)
                  })
                }}
                defaultValue={this.state.passwordRepeated}
              />
              {(this.state.PasswordStrengthRepeated !== "") && <Text style={styles.ErrorMessageStyle}>{this.state.PasswordStrengthRepeated}</Text>}
              {(this.state.isPasswordRepeatedOK == false || this.state.ErrorMessagePasswordRepeated != "") && <Text style={styles.ErrorMessageStyle}>{this.state.ErrorMessagePasswordRepeated}</Text>}

              <TouchableOpacity
                style={styles.button}
                onPress={this.handleMAJ}
              >
                <Text>Mettre à jour</Text>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity
                style={styles.button}
                onPress={this.goToConnexionPage}
              >
                <Text>Déconnexion</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logo: {
    alignSelf: 'center',
    width: 100,
    height: 80,
    marginTop: 60,
    marginBottom: 40
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
    fontWeight: 'bold',
    fontSize: 32
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#A4BBE8',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    padding: 20,
    margin: 10
  },
  input: {
    height: 40,
    margin: 10,
    borderWidth: 1,
    borderColor: '#ABA9AB'
  },
  inputPassword: {
    height: 40,
    margin: 10,
    borderWidth: 1,
    borderColor: '#ABA9AB'
  },
  scrollView: {
    marginHorizontal: 0,
    backgroundColor: '#fff',
    padding: 20,
    margin: 10,
    marginBottom: 50
  },
  avatar_container: {
    alignItems: 'center',
  },
  datePickerStyle: {
    height: 40,
    margin: 10,
    width: 300
  },
  ErrorMessageStyle: {
    color: '#FF0000'
  }
})
