import React, { useState, useEffect } from 'react'
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Button,
  StatusBar,
  ActivityIndicator,
  RefreshControlComponent,
} from 'react-native'
import 'react-native-gesture-handler'
import 'localstorage-polyfill'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';


import { NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createSwitchNavigator, createAppContainer } from 'react-navigation'

//screens
import ConnexionScreen from './components/ConnexionScreen'
import InscriptionScreen from './components/InscriptionScreen'
import WelcomePage from './components/WelcomePage'
import ProfilePage from './components/ProfilePage'
import MyDocumentsPage from './components/MyDocumentsPage';
import AdmissionPage from './components/AdmissionPage';
import authService from './services/auth-service';

// screens names
const inscriptionName = 'Inscription'
const connexionName = 'Connexion'
const welcomeName = 'WelcomePage'
const profileName = 'ProfilePage'
const documentName = 'MyDocumentsPage'
const admissionName = 'AdmissionPage'

// npm install --save react-native-gesture-handler react-native-reanimated react-native-screens
const Tab = createBottomTabNavigator()
const Stack = createStackNavigator()

const SignedOut = () => {
  return (
    <Stack.Navigator screenOptions={{
      headerShown: false
    }}>
      <Stack.Screen name="Connexion" component={ConnexionScreen} />
      <Stack.Screen name="Inscription" component={InscriptionScreen} />
      <Stack.Screen name="SignedIn" component={SignedIn} />
      <Stack.Screen name="SignedOut" component={SignedOut} />
    </Stack.Navigator>
  )
}

const SignedIn = ({ navigation }) => {
  return (
    <Tab.Navigator screenOptions={{
      tabBarStyle: { position: 'absolute' },
      tabBarActiveTintColor: 'blue', headerShown: false
    }}>
      <Tab.Screen options={{
        tabBarLabel: 'Home',
        headerShown: false,
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="home" color={color} size={size} />
        ),
      }} name={welcomeName} component={WelcomePage} />
      <Tab.Screen options={{
        tabBarLabel: 'My Documents',
        headerShown: false,
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="file-document" color={color} size={size} />
        ),
      }} name={documentName} component={MyDocumentsPage} />
      <Tab.Screen options={{
        tabBarLabel: 'My Admissions',
        headerShown: false,
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="ambulance" color={color} size={size} />
        ),
      }} name={admissionName} component={AdmissionPage} />
      <Tab.Screen options={{
        tabBarLabel: 'My Profile',
        headerShown: false,
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="face-profile" color={color} size={size} />
        ),
      }} name={profileName} component={ProfilePage} />
    </Tab.Navigator>
  )
}

export default class AppNavigator extends React.Component {
  constructor(props) {
    super(props)
    let isAuth;
  }

  render() {
    if (localStorage.getItem('userToken')) {
      isAuth = true;
    }
    else
      isAuth = false;
    return (
      <NavigationContainer>
        {isAuth && <SignedIn />}
        {!isAuth && <SignedOut />}
      </NavigationContainer>)
  }
};
