import axios from 'axios';
// import * as Network from 'expo-network';
import { REACT_APP_API_SERVER, REACT_APP_API_SERVER_LOCAL, REACT_APP_API_PORT, REACT_APP_ENVIRONNEMENT } from "@env";
import Constants from 'expo-constants';
// var RNFS = require('react-native-fs'); // Lib Pour gerer les fichiers sous React native
// import RNFS from 'react-native-fs';
import * as FileSystem from 'expo-file-system';
import { IntentLauncher } from 'expo-intent-launcher';

const {
  manifest
} = Constants;

let serverURL = REACT_APP_API_SERVER_LOCAL;
let API_URL = `http://${serverURL}:${REACT_APP_API_PORT}`;

if (REACT_APP_ENVIRONNEMENT == "developpement") {
  // const ip = Network.getIpAddressAsync();
  // ip.then((e) => {
  //     serverURL = e;
  //     API_URL = `http://${serverURL}:${REACT_APP_API_PORT}`;
  // }
  // );
  API_URL = `http://${manifest.debuggerHost
    .split(`:`)
    .shift()
    .concat(`:${REACT_APP_API_PORT}`)}`; // Switch to the port you use. ex)localhost:3000 => .concat(`:3000`)}
}
// const SERVER = REACT_APP_API_SERVER ? REACT_APP_API_SERVER : REACT_APP_API_SERVER_LOCAL;


// class AuthService {
//     login(email, password) {
//         return axios
//             .post(`${API_URL}/users/login`, {
//                 email,
//                 password
//             })
//             .then(response => {
//                 if (response.data.content.token) {
//                     localStorage.setItem('token', response.data.content.token);
//                 }

class AuthService {
  constructor() {
    this.state = {
      successful: false,
      myuser: ""
    }
  }
  login(email, password) {
    this.getCsrfToken();
    return axios.post(`${API_URL}/users/login`, {
      email,
      password,
    })
  }

  isSignedIn() {
    const token = localStorage.getItem('userToken')
    if (token !== null) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('userToken')
  }

  register(email, password, firstname, lastname, birthdate, NSS, profilImg, role_set) {
    const role = role_set ? role_set : 'ROLE_USER';
    return axios.post(`${API_URL}/users/register`, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      email,
      password,
      firstname,
      lastname,
      num_ss: NSS,
      birthdate,
      profilImg: profilImg,
      role
    });
  }

  async update(id, email, password, firstname, lastname, birthdate, NSS, profilImg) {
    let data = {
      id: id,
      email: email,
      password: password,
      firstname: firstname,
      lastname: lastname,
      birthdate: birthdate,
      num_ss: NSS,
      profilImg: profilImg,
    };

    const dataupdated = await axios.put(`${API_URL}/users/profile`, {
      data,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
    });
    if (dataupdated.status === 200) {
      return dataupdated.data
    } else {
      return 'Bad Token'
    }
  }

  async getCurrentUser() {
    const token = localStorage.getItem('userToken')
    const csrfToken = localStorage.getItem('csrf')
    
    if (token && csrfToken) {
      try {
        const data = await axios.get(`${API_URL}/users/profile`, {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'CSRF-Token': csrfToken,
            'X-CSRF-Token': csrfToken,
            Authorization: `Bearer ${token}`,
          },
        })

        if (data.status === 200) {
          return data.data.content
        } else {
          return 'Bad Token'
        }
      } catch (err) {
        return 'Bad Token'
      }
    } else {
      return 'Bad Token'
    }
  }

  async getUserLinkedImg() {
    const token = localStorage.getItem('userToken')
    const csrfToken = localStorage.getItem('csrf')
    if (token && csrfToken) {
      try {
        const data = await axios.get(`${API_URL}/users/profile`, {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'CSRF-Token': csrfToken,
            'X-CSRF-Token': csrfToken,
            Authorization: `Bearer ${token}`,
          }
        })
        let download = FileSystem.createDownloadResumable(`${API_URL}/users/photo`,
          FileSystem.documentDirectory + data.data.content.user.profilImg, {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'CSRF-Token': csrfToken,
            'X-CSRF-Token': csrfToken,
            Authorization: `Bearer ${token}`
          }
        }
        );
        await download.downloadAsync();
        let finalFile = await FileSystem.getInfoAsync(FileSystem.documentDirectory + data.data.content.user.profilImg);
        if (finalFile) {
          data.data.content.user.userImageUri = finalFile.uri;
        }
        if (data.status === 200) {
          return data.data.content;
        } else {
          return 'Bad Token';
        }
      } catch (err) {
        return 'Bad Token';
      }
    } else {
      return 'Bad Token';
    }
  }

  async getCsrfToken() {
    try {
      const data = await axios.get(`${API_URL}/csrf`, {})
      if (data.status === 200) {
        if (data.data.content.csrfToken) {
          localStorage.setItem('csrf', data.data.content.csrfToken)
          return 'Good Token'
        }
      } else {
        return 'Bad Token'
      }
    } catch (err) {
      return 'Bad Token'
    }
  }

  async sendProfilePicture(files, userMail, fileType = "") {
    const result = await FileSystem.uploadAsync(
      `${API_URL}/users/update_photo`,
      files[0].filepath,
      {
        Filetype: fileType,
        uploadType: FileSystem.FileSystemUploadType.MULTIPART,
        fieldName: 'userImage',
        mimeType: 'image/png',
        parameters: {
          userMail: userMail,
          type_image: 'mutuelle',
        },
      }
    );
    return result;
  }

  async createDocuments(data) {
    return await axios.post(`${API_URL}/docs/update_doc`, {
      headers: {
        'Content-Type': data.document_blob && data.document_blob.type,
      },
      data
    }).then(response => {
      console.log("message ", response.data.message)
    });
  }

  async getAllDocs(id_user) {
    let data = [];
    await axios.get(`${API_URL}/docs/getdocs/${id_user}`).then(response => data = response.data);
    
    if (data != null && data != undefined) {
      return data
    } else {
      return 'Bad Token'
    }
  }

  async deleteDocuments(id_doc) {
    return await axios.delete(`${API_URL}/docs/${id_doc}`).then(response => console.log(response.data.message));
  }
  

}
export default new AuthService()
